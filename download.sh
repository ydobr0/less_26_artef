#!/bin/bash
OUTPATH="my_art"
CURL=$(which curl)
L_TOKEN="jd5YgPiD6qmtxzmpfMAu"
PROJ="23693176"
#get pipeline_id
PIP_ID=$( $CURL -k --location --header "PRIVATE-TOKEN: ${L_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJ}/pipelines" | jq '[.[]|.id]| max')
echo "$PIP_ID"
#get job_id
J_ID=$($CURL -k --location --header "PRIVATE-TOKEN: ${L_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJ}/pipelines/${PIP_ID}/jobs" | jq '.[] | select (.name =="build_dev")|.id')
echo $J_ID
#download art
$CURL --output ${OUTPATH} --location --header "PRIVATE-TOKEN: ${L_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJ}/jobs/${J_ID}/artifacts/build.log"
ls -la
cat my_art
